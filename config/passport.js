/**
 * Passport configuration file where you should configure strategies
 */

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
  function (email, password, done) {
    User.findOne({
      email: email
    }).exec(function (err, user) {

      if (err) {
        return done(null, err);
      }
      if (!user) {
        return done(null, false, {
          message: 'Invalid email or Password'
        });
      }
      bcrypt.compare(password, user.password, function (err, res) {
        if (err || !res) {
          return done(null, false, {
            message: 'Invalid email or Password'
          });
        }
        return done(null, user);
      });
    });
  }));
