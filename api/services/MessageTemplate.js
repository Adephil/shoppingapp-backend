module.exports = (products) => {
  const template = products.map(product => {
    return `${product.cartCount} ${product.name}\n`
  }).join('');
  return `Here are your orders:\n${template}`;
}
