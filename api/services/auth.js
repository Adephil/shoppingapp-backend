const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = {
  login: function (req, res) {
    passport.authenticate('local', function (err, user, info) {
      if (!user) {
        res.status(400).send({
          success: false,
          error: info.message
        });
        return;
      } else {
        if (err) {
          res.status(400).send({
            success: false,
            error: info.message
          });
        } else {
          //token expired in 1 day
          var token = jwt.sign(user, sails.config.secret, {
            expiresIn: 60 * 60 * 24
          });
          console.log("user", user);
          res.send({
            success: true,
            user: {
              firstname: user.firstname,
              token: token
            }
          });
        }
      }
    })(req, res);
  },
  isvalidtoken: function (token) {
    if (token) {
      return jwt.verify(token.replace('Bearer ', ''), sails.config.secret);
    }
  }
};
