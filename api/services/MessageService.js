const twilio = require('twilio');

module.exports = (message) => {
  const accountSid = process.env.SMSID; // Your Account SID from www.twilio.com/consoley
  const authToken = process.env.SMSTOKEN;   // Your Auth Token from www.twilio.com/console
  const fromNumber = process.env.SMSFROMNUMBER;
  const toNumber = process.env.SMSTONUMBER; //can only receive text on this number.
  const client = new twilio(accountSid, authToken);

  return client.messages.create({
    body: message,
    to: toNumber,  // Text this number
    from: fromNumber // From a valid Twilio number
  })
}
