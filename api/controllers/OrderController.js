const sendSms = require('../services/MessageService');
const smsTemplate = require('../services/MessageTemplate');
const auth = require('../services/auth');

module.exports = {
  order: async (req, res) => {
    const { email, products } = req.body;

    try {
      console.log("req", req.headers.authorization)
      const user = await auth.isvalidtoken(req.headers.authorization);

      console.log("user", user);

      if (user) {
        ItemsPromise = products.map(product => {
          return Item.create({
            productId: product.id,
            qty: product.cartCount
          }).fetch();
        });

        Promise.all(ItemsPromise).then((items) => {
          const itemIds = items.map(item => item.id);

          Order.create({ user: user.id, items: itemIds }).fetch().then((userOrder) => {
            console.log("products", products);
            const message = smsTemplate(products);
            sendSms(message).then((response) => {
              if (response.errorCode) {
                res.json({
                  status: 'failed',
                  statusCode: 500,
                  message: 'could not send sms'
                });
              }
            }).catch(error => {
              console.log("error", error);
              res.json({
                status: 'failed',
                statusCode: 200,
                message: 'could not send sms'
              });
            });
            res.json({
              status: 'success',
              statusCode: 200,
              order: userOrder
            })
          });

        }).catch(error => console.log(error));

      } else {
        res.send(500, {
          error: 'No user'
        })
      }
    } catch (error) {
      res.status(500).send({
        error: 'Could not fetch data'
      });
    }

  }
}