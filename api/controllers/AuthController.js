var auth = require('../services/auth');

module.exports = {
  register: function (req, res) {
    auth.register(req, res);
  },
  login: function (req, res) {
    auth.login(req, res);
  },
  validatetoken: function (req, res) {
    auth.isvalidtoken(req, res);
  },
  logout: function (req, res) {
    req.logout();
    req.session.destroy();
    res.send(200);
  }
};
