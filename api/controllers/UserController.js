module.exports = {
  post: function (req, res) {
    const email = req.body.email;
    const password = req.body.password;
    const phonenumber = req.body.phonenumber;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    //validate request
    if (_.isUndefined(req.param('email'))) {         
      return res.badRequest('An email address is required!');    
    }
    if (_.isUndefined(req.param('password'))) {
      return res.badRequest('A password is required');
    }
    if (req.param('password').length < 6) {
      return res.badRequest('A password must be at least 6 character')
    }

    User.findOne({
      email: email
    }).exec(function (err, result) {
      //validate from database
      if (err) {
        return res.serverError(err);
      } else if (result) {
        return res.badRequest('Email already used!');
      } else {

        User.create({
          email,
          password,
          phonenumber,
          firstname,
          lastname
        }).exec(function (err, result) {
          if (err) {
            return res.serverError(err);
            //return res.badRequest('Error create user');
          }
          return res.ok();
        })
      }
    });


  },
  get: async (req, res) => {
    try {
      const users = await User.find({});
      res.json({
        status: 'success',
        statusCode: 200,
        data: users
      })
    } catch (error) {
      res.send(500, {
        error: 'Could not fetch data'
      })
    }
  },
  findOne: async (req, res) => {
    try {
      const products = await Product.find({
        _id: req.params.id
      });
      res.json({
        status: 'success',
        statusCode: 200,
        data: products
      })
    } catch (error) {
      res.send(500, {
        error: 'Could not fetch data'
      })
    }
  },
  put: async (req, res) => {

    try {
      const product = await Product.update({
        _id: req.params.id
      }, req.body);

      res.json({
        status: 'success',
        statusCode: 200,
        data: product
      });
    } catch (error) {
      res.send(500, {
        error: 'Could not Add data'
      })
    }
  },
  delete: async (req, res) => {
    console.log(req.params.id);
    try {
      await Product.destroy({
        id: req.params.id
      })

      res.json({
        status: 'success',
        statusCode: 200,
      });
    } catch (error) {
      res.send(500, {
        error: 'Could not delete data'
      })
    }

  },
}
