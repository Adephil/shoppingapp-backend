/**
 * CartController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  getItems: async (req, res) => {
    const email = req.user.email;
    try {
      const cart = await Cart.findOne({
        email
      }).populate('items');
      const count = cart.items.reduce((sum, item) => sum += item.qty, 0);
      res.json({
        status: 'success',
        statusCode: 200,
        count: count
      })
    } catch (error) {
      res.send(500, {
        error: 'Could not fetch data'
      })
    }
  },
  post: async (req, res) => {
    const email = req.user.email;
    const productId = req.body.productId;

    const cart = await Cart.findOne({
      email: email
    }).populate('items');
    console.log(cart);
    if (cart) {
      const indexFound = cart.items.findIndex(item => {
        return item.productId === productId;
      });
      if (indexFound != -1) {
        await Item.update({
          id: cart.items[indexFound].id
        }, {
          qty: ++cart.items[indexFound].qty
        })
      } else {
        const newItem = await Item.create({
          productId,
          qty: 1
        }).fetch();

        cart.items.push(newItem);
        const itemIds = cart.items.map(item => item.id);
        newCart = await Cart.update({
          id: cart.id,
        }, {
          items: itemIds
        });


      }

    } else {
      const newItem = await Item.create({
        productId,
        qty: 1
      }).fetch();

      newCart = await Cart.create({
        email: email,
        items: [newItem.id]
      });

    }

    res.json({
      message: 'ok'
    })

  }
}
