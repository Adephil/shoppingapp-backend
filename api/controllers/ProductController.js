/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  get: async (req, res) => {
    try {
      const products = await Product.find({});
      res.json({
        status: 'success',
        statusCode: 200,
        data: products
      })
    } catch (error) {
      res.send(500, {
        error: 'Could not fetch data'
      })
    }
  },
  findOne: async (req, res) => {
    try {
      const products = await Product.find({
        _id: req.params.id
      });
      res.json({
        status: 'success',
        statusCode: 200,
        data: products
      });
    } catch (error) {
      res.send(500, {
        error: 'Could not fetch data'
      })
    }
  },
  post: async (req, res) => {
    const name = req.body.name;
    const description = req.body.description;
    const price = req.body.price;
    const quantity = req.body.quantity;
    const imagePath = req.body.imagePath;



    try {
      const products = await Product.create({
        name,
        description,
        price,
        quantity,
        imagePath
      });
      res.json({
        status: 'success',
        statusCode: 200,
        data: products
      })
    } catch (error) {
      res.send(500, {
        error: 'Could not Add data'
      })
    }
  },

  put: async (req, res) => {

    try {
      const product = await Product.update({
        _id: req.params.id
      }, req.body);

      res.json({
        status: 'success',
        statusCode: 200,
        data: product
      });
    } catch (error) {
      res.send(500, {
        error: 'Could not Add data'
      })
    }
  },
  delete: async (req, res) => {
    console.log(req.params.id);
    try {
      await Product.destroy({
        id: req.params.id
      })

      res.json({
        status: 'success',
        statusCode: 200,
      });
    } catch (error) {
      res.send(500, {
        error: 'Could not delete data'
      })
    }

  },

};
