const expressJwt = require('express-jwt');
const secret = sails.config.secret;

module.exports = expressJwt({
  secret: secret
});
