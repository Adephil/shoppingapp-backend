module.exports = (req, res, next) => {
  const isAdmin = req.user.isAdmin;

  if (!isAdmin) {
    return res.status(401).send({
      "name": "UnauthorizedError",
      "message": "Not an admin",
      "code": "credentials_required",
      "status": 401,
      "inner": {
        "message": "Not an admin"
      }
    });
  }
  next();
}
